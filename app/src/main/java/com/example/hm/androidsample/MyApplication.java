package com.example.hm.androidsample;

import android.app.Application;

import com.squareup.leakcanary.LeakCanary;

/**
 * Created by HM on 2015/10/23.
 */
public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        LeakCanary.install(this);
    }
}
