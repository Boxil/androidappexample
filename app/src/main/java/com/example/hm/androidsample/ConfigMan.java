package com.example.hm.androidsample;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by HM on 2015/10/23.
 */
public class ConfigMan {

    private static final String KEY_THRESHOLD = "KEY_THRESHOLD";

    public static void writeThreshold(Context context, float val) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putFloat(KEY_THRESHOLD, val);
        editor.commit();
    }

    public static float readThreshold(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getFloat(KEY_THRESHOLD, 0);
    }
}
