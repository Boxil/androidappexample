package com.example.hm.androidsample;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import java.util.List;

import static android.content.Context.SENSOR_SERVICE;

/**
 * Created by HM on 2015/10/13.
 */
public class LightSensorManager implements SensorEventListener {
    private SensorManager mLightSensorManager;
    private Context mContext;
    volatile private float val;

    public LightSensorManager(Context context) {
        mContext = context;
        mLightSensorManager = (SensorManager) mContext.getSystemService(SENSOR_SERVICE);
    }

    public float getVal() {
        return val;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        val = event.values[0];
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void regster() {
        List<Sensor> sensorList = mLightSensorManager.getSensorList(Sensor.TYPE_LIGHT);
        if (sensorList.size() > 0) {
            mLightSensorManager.registerListener(this, sensorList.get(0), SensorManager.SENSOR_DELAY_FASTEST);

        }
        Log.d("SMGR", "registered.");
    }

    public void unregister() {
        mLightSensorManager.unregisterListener(this);
        Log.d("SMGR", "unregistered.");

    }
}
