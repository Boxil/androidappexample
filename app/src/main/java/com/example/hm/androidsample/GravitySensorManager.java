/**
 * @author boxil
 * @since 2015/10/25
 * Callbackで呼び出してみる
 */

package com.example.hm.androidsample;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import java.util.List;

import static android.content.Context.SENSOR_SERVICE;

public class GravitySensorManager implements SensorEventListener {

    private SensorManager mGravitySensorManager;
    private Context mContext;
    private Callbacks mCallback;

    public GravitySensorManager(Context context, Callbacks callback) {
        mContext = context;
        mCallback = callback;
        mGravitySensorManager = (SensorManager) mContext.getSystemService(SENSOR_SERVICE);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        mCallback.callbackMethod(event.values[0]);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void regster() {
        List<Sensor> sensorList = mGravitySensorManager.getSensorList(Sensor.TYPE_LIGHT);
        if (sensorList.size() > 0) {
            mGravitySensorManager.registerListener(this, sensorList.get(0), SensorManager.SENSOR_DELAY_FASTEST);
        }
        Log.d("SMGR", "registered.");
    }

    public void unregister() {
        mGravitySensorManager.unregisterListener(this);
        Log.d("SMGR", "unregistered.");
    }

    public interface Callbacks {
        void callbackMethod(float val);
    }
}
