package com.example.hm.androidsample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class ConfigActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
        findViewById(R.id.button).setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        TextView text = (TextView) findViewById(R.id.thresholdEditText);
        text.setText(String.valueOf(ConfigMan.readThreshold(this)));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button:
                TextView text = (TextView) findViewById(R.id.thresholdEditText);
                ConfigMan.writeThreshold(this, Float.parseFloat(text.getText().toString()));
                Toast.makeText(this, "Save.", Toast.LENGTH_SHORT).show();
                break;
            default:
        }
    }
}
