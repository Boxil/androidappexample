package com.example.hm.androidsample;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity implements GravitySensorManager.Callbacks {
    private final String TAG_MAINACTIVITY = "TAG_MAINACTIVITY";

    SensorAsyncTask mSensorAsyncTask;
    GravitySensorManager mGravitySensorManager;

    float mThreshold = 0.0f;

    @Override
    protected void onResume() {
        super.onResume();
        mThreshold = ConfigMan.readThreshold(this);
        mSensorAsyncTask = new SensorAsyncTask(this);
        mSensorAsyncTask.execute("params");
        mGravitySensorManager.regster();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mSensorAsyncTask.cancel(true);
        mSensorAsyncTask = null;
        mGravitySensorManager.unregister();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mGravitySensorManager = new GravitySensorManager(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, ConfigActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void callbackMethod(float val) {
        Log.d(TAG_MAINACTIVITY, "GravityVal=" + val);
    }

    private class SensorAsyncTask extends AsyncTask {
        TextView mTextView = (TextView) findViewById(R.id.sensorText);
        ImageView mImageView = (ImageView) findViewById(R.id.imageView);
        LightSensorManager mLigthSensorManager;

        public SensorAsyncTask(Context context) {
            mLigthSensorManager = new LightSensorManager(context);
            mLigthSensorManager.regster();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            mLigthSensorManager.unregister();
            mLigthSensorManager = null;
            Log.d(TAG_MAINACTIVITY, "Task is Canceld.");
        }

        @Override
        protected Object doInBackground(Object[] params) {
            while (!isCancelled()) {
                publishProgress();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Object[] values) {
            super.onProgressUpdate(values);
            float val = mLigthSensorManager.getVal();
            String text = Float.toString(val);
            if (val > mThreshold) {
                text = text + "\t閾値以上(" + mThreshold + ")";
            } else {
                text = text + "\t閾値以下(" + mThreshold + ")";
            }
            mTextView.setText(text);
        }
    }
}
