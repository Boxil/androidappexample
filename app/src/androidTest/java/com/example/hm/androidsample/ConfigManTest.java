package com.example.hm.androidsample;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.test.AndroidTestCase;

/**
 * Created by HM on 2015/10/24.
 */
public class ConfigManTest extends AndroidTestCase {

    public ConfigManTest() {
        super();
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testWriteThreshold() throws Exception {
        float val = 1.0f;
        Context context = getContext();
        ConfigMan.writeThreshold(context, val);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        float ret = sp.getFloat("KEY_THRESHOLD", 100.0f);
        assertEquals(val, ret);
    }

    public void testReadThreshold() throws Exception {
        Context context = getContext();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        float val = sp.getFloat("KEY_THRESHOLD", 100.0f);
        float ret = ConfigMan.readThreshold(context);
        assertEquals(val, ret);
    }
}